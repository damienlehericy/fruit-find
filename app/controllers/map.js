var apiClient = require('get_api'),
	markers = [],
	$ = $;
	Ti.App.itemId = '1';

// Geolocation
if (Ti.Geolocation.locationServicesEnabled) {
	refreshMap();
}
Ti.App.addEventListener('tree_change', function() {
	refreshMap();
});
function refreshMap () {
	Titanium.Geolocation.distanceFilter = 150;
	Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
	Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;
    Titanium.Geolocation.purpose = 'Get Current Location';
    
    Titanium.Geolocation.addEventListener('location', function(e) {
        if (e.error) {
            alert('Error: ' + e.error);
        } else {
			var me_marker =  Alloy.Globals.Map.createAnnotation({
				title: "You",
			    latitude: e.coords.latitude,
			    longitude: e.coords.longitude,
			    pincolor:  Alloy.Globals.Map.ANNOTATION_PURPLE,
			    myid:1 // Custom property to uniquely identify this annotation.
			});
			markers.push(me_marker);
			
			apiClient.defineRequest('fruit','fruit/' + Ti.App.itemId ,'GET', function (data) {
				"use strict";
				$.map_module.removeAllAnnotations();
			    _.each(data, function(tree) {
			        var annotation = Alloy.Globals.Map.createAnnotation({
			            latitude: tree.x,
			            longitude: tree.y,
			            title: tree.title,
			            pincolor: Alloy.Globals.Map.ANNOTATION_GREEN
			        });
			        markers.push(annotation);
			    });
				$.map_module.setRegion({
			        latitude: e.coords.latitude,
			        longitude: e.coords.longitude,
			        animate:true,
			        latitudeDelta: 1,
			        longitudeDelta: 1
			    });
			    $.map_module.setAnnotations(markers);
			});
        }
    });
}
