var apiClient = require('get_api'),
	$ = $;
setTimeout(function () {
	apiClient.defineRequest('fruit','trees' ,'GET', function (data) {
		"use strict";
		var items = [],
			listView = Ti.UI.createListView();
		_.each(data, function (tree) {
			items.push({
				properties : {
					template: 'template',
					itemId: tree.id,
		            title: tree.name + ", numbers: " + tree.count,
		            color: tree.color
				}
			});
		});
		var section = Ti.UI.createListSection(),
			self = $;
		section.setItems(items);
		listView.sections = [section];
		listView.addEventListener('itemclick', function(e){
			Ti.App.itemId = e.itemId;
			Ti.App.fireEvent('tree_change');
		});
		$.trees_window.add(listView);
	});
}, 5000);