exports.parseResponse = function (response) {
	"use strict";
	return JSON.parse(response);
};