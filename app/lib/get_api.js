var url,
	api,
	client = Ti.Network.createHTTPClient(),
	fruitApi = require('api_fruit'),
	ratpApi = require('api_ratp'),
	response;	

exports.defineRequest = function (api, request, method, callback) {
	"use strict";
	
	switch(api) {
	case 'fruit':
		url = 'http://' + 'www.fruitmap.sk/api/' + request;
	break;
	case 'ratp':
		url = 'http://' + '' + request;
	break;
	}

    client.onload = function () {
		if (api === "fruit") {
			response = fruitApi.parseResponse(this.responseText);
			callback(response);
		} else {
			response = ratpApi.parseResponse(this.responseText);
			callback(response);
		}
	};

    client.onerror = function (e) {
        response = "Error to called api: " + e.error;
        callback(response);
    };

    client.open(method, url, true);
    client.send();
};
